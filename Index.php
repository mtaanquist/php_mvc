<?php

// Routing is domain.tld/{controller}/{action}/{id}?
// Translated to domain.tld/?controller={controller}&action={action}(&id={id})?

// Default controller and action is 'Home' and 'Index'
$controller = (isset($_GET['controller'])) ? $_GET['controller'] : 'Home';
$action = (isset($_GET['action'])) ? $_GET['action'] : 'Index';
$id = (isset($_GET['id'])) ? $_GET['id'] : null; // Optional

$controllerClassName = '\\Controllers\\' . ucfirst($controller) . 'Controller';
if (class_exists($controllerClassName))
{
	$controller = new $controllerClassName;
	if ((strlen($action) > 0) && ($action[0] != '_') && method_exists($controller, $action) 
		&& is_callable(array($controller, $action)))
	{
		// Validation of $id happens in $controller as it is context-specific
		$html = $controller->$action($id);

		if (!empty($html)) echo $html;
		else http_response_code(400);
		
		exit;
	}
	else
	{
		http_response_code(400);
		exit;
	}
}
else
{
	http_response_code(404);
	exit;
}

function __autoload($className)
{
	$pattern = '/^(\\\\?[A-Z]{1,30})+$/i';
	if (preg_match($pattern, $className) === 1)
	{
		$filepath = str_replace('\\', '/', $className) . '.php';
		if (file_exists($filepath)) require_once($filepath);
		else echo 'File not found.';
	}
}