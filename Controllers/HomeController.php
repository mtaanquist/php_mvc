<?php

namespace Controllers 
{
	class HomeController
	{
		public function Index()
		{
			$vars = array(
				'title' => 'Home',
				'content' => 'Woooooooo'
			);

			$layout = file_get_contents('Views/Shared/_layout.php');

			$html = preg_replace_callback(
				'/\{\$(.*?)\}/si', function ($matches) use ($vars) 
				{
					// matches[0] is the entire match, where matches[1] is just the captured group
					// which we'll use for the array's key
					return str_replace($matches[0], (isset($vars[$matches[1]])) ? $vars[$matches[1]] : $matches[0], $matches[0]);
				}, 
				$layout
			);

			return $html;
		}
	}
}